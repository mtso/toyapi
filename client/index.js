import React from 'react'
import { hydrate } from 'react-dom'
import { BrowserRouter as Router } from 'react-router-dom'
import { Provider } from 'react-redux'

import App from '../app'
import trackReferrer from './trackReferrer'
import store from './clientStore'
import './style/main.scss'

hydrate(
  <Router>
    <Provider
      store={store}
    >
      <App />
    </Provider>
  </Router>,
  document.querySelector('#app'),
)

if (document.location.pathname === '/') {
  const {referrer} = document
  trackReferrer(referrer)
}
