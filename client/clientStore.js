import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import rootReducer from '../app/reducers'

const initialState = window.__PRELOADED_STATE__ || null

export default createStore(
  rootReducer,
  initialState,
  applyMiddleware(thunk),
)
