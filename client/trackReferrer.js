/*global mixpanel b:true*/

export default function trackReferrer(referrer) {
  mixpanel
    .track(
      'referrer',
      {referrer},
    )
}
