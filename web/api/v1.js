import { Router } from 'express'
import problem from '../routes/problem'
import test from '../routes/test'

const api = Router()

api.use('/problem', problem)
api.use('/', test)
api.use('/*', (req, res, next) => {
  res.status(404)
    .json({
      message: 'Not found',
    })
})

export default api
