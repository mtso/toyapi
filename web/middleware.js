import express from 'express'
import cookieParser from 'cookie-parser'
import bodyParser from 'body-parser'
import githubStrategy from './auth/github'
import auth from './auth'
import session from 'express-session'
import redirectHttp from './middleware/redirectHttp'

const middleware = [
  // TODO: update static path to `/static/*`
  express.static('dist'),
  cookieParser(),
  bodyParser.json(),
  bodyParser.urlencoded({extended: true}),

  session({
    secret: process.env.SESSION_SECRET,
    saveUninitialized: true,
    resave: true,
  }),

  auth.initialize(),
  auth.session(),
]

if (process.env.NODE_ENV !== 'development') {
  middleware.unshift(redirectHttp)
}

export default middleware
