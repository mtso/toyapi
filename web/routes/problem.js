import { Router } from 'express'
import Problem from '../models/Problem'
import {
  fetchProblemListState,
  fetchProblemState,
} from './fetch'
import getSpecMarkup from '../utils/getSpecMarkup'

const api = Router()

api.get(
  '/:id',
  fetchProblemState,
  (req, res, next) => {
    const { problem } = res.locals.state

    // TODO: do this on the model as a virtual
    problem.specMarkup = getSpecMarkup(problem.filename)
    res.json(problem)
  },
)

api.get(
  '/',
  fetchProblemListState,
  (req, res, next) => {
    const { problemList } = res.locals.state

    const withMarkup = problemList.map((p) => {
      p.specMarkup = getSpecMarkup(p.filename)
      return p
    })

    res.json(withMarkup)
  },
)

api.get(
  '/:id/spec',
  (req, res, next) => {

  },
)

export default api
