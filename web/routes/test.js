import { Router } from 'express'
import TestRequest from '../models/TestRequest'
import User from '../models/User'
import TestResult from '../models/TestResult'
import dynamoose from 'dynamoose'
import genId from '../utils/genId'
import AWS from 'aws-sdk'
import { isAuthenticated, isAuthorized } from '../auth'
import { DEV, ADMIN } from '../config/scopes'
import logger from '../logger'
import { fetchProblemResultsState } from './fetch'

const REQUEST_RATE = 30 * 1000 // 30 seconds

function rateLimit(delay) {
  const cache = {}

  return (req, res, next) => {
    if (!req.user.id) {
      next(new Error('Unauthenticated'))
      return
    }

    const previousTime = cache[req.user.id]
    if (!previousTime) {
      cache[req.user.id] = Date.now()
      next()
      return
    }

    const diff = Date.now() - previousTime
    if (diff < delay) {
      cache[req.user.id] = Date.now()
      next()
    } else {
      res.json({
        message: 'Please wait for rate limit to refresh',
        'time_left': delay - diff,
      })
    }
  }
}

const test = Router()

test.get(
  '/pending',
  isAuthenticated,
  (req, res, next) => {
    const id = req.query.id || req.user.id

    User
      .get(id)
      .then((user) => {
        const pending = user.requests || []

        res.json(pending)
      })
      .catch(next)
  },
)

test.get(
  '/test/:id',
  isAuthenticated,
  fetchProblemResultsState,
  (req, res, next) => {
    res.json(res.locals.state.problem.results)
  },
)

// /api/test/abc123/
// get user id from req
test.post(
  '/test/:id',
  isAuthenticated,
  // isAuthorized([DEV, ADMIN]),

  rateLimit(REQUEST_RATE),

  (req, res, next) => {
    const problemId = req.params.id
    const endpoint = req.body.endpoint
    const userId = req.user.id
    const requestId = genId()

    const request = new TestRequest({
      id: requestId,
      'problem_id': problemId,
      endpoint,
      'user_id': userId,
      'problemid_userid': problemId + ':' + userId,
    })

    const ddb = new AWS.DynamoDB.DocumentClient()
    const params = {
      TableName: 'ToyApi-User',
      Key: {id: userId},
      UpdateExpression: 'ADD requests :req_id',
      ExpressionAttributeValues: {
        ':req_id': ddb.createSet([requestId]),
      },
      ReturnValues: 'ALL_NEW',
    }

    ddb
      .update(params, (err, data) => {
        if (err) {
          next(err)
          return
        }

        request
          .save()
          .then(() => {
            res.json(request)
          })
          .catch((err) => {
            logger.error(err.message)
            next()
          })
      })
  },
)

export default test
