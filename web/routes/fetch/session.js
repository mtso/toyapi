export const fetchSessionState = (req, res, next) => {
  res.locals.state = res.locals.state || {}
  res.locals.state.session = {
    username: req.user && req.user.username || null,
    id: req.user && req.user.id || null,
  }
  next()
}
