import Problem from '../../models/Problem'
import TestResult from '../../models/TestResult'
import { DEV, ADMIN } from '../../config/scopes'
import getSpecMarkup from '../../utils/getSpecMarkup'

const listCache = {}
const PROBLEM_KEY = 'PROBLEMLIST'

function getAllProblems(req) {
  const cacheKey = PROBLEM_KEY// + (req.isAuthenticated() ? req.user.scopes.join() : '')

  if (listCache[cacheKey]) {
    return new Promise((resolve, _) => resolve(listCache[cacheKey]))
  }

  return Problem
    .scan()
    .all(0.2)
    .exec()
    .then((problems) => {
      problems = problems
        .sort((a, b) => {
          return a.order - b.order
        })
        .map((p) => {
          p.specMarkup = getSpecMarkup(p.filename)
          return p
        })

      listCache[cacheKey] = problems
      return problems
    })
}

export const fetchProblemListState = (req, res, next) => {
  // max 30 results
  const pageCount = Math.min(30, req.query['page_count'] || 10)
  const page = Math.max(1, (req.query.page && req.query.page) || 1)

  let scan = getAllProblems(req)

  const canViewUnpublished = req.isAuthenticated() &&
    (req.user.scopes.includes(DEV) || req.user.scopes.includes(ADMIN))

  if (!canViewUnpublished) {
    scan = scan.then((problems) => {
      return problems.filter((p) => p.is_published)
    })
  }

  scan
    .then((problems) => problems.slice(page - 1, page - 1 + pageCount))
    .then((problems) => {
      res.locals.state = res.locals.state || {}
      res.locals.state.problemList = problems
      next()
    })
    .catch(next)
}

export const fetchProblemState = (req, res, next) => {
  const problemId = req.params.id

  Problem
    .get(problemId)
    .then((problem) => {
      res.locals.state = res.locals.state || {}

      problem.specMarkup = getSpecMarkup(problem.filename)
      res.locals.state.problem = problem
      next()
    })
    .catch(next)
}

export const fetchProblemResultsState = (req, res, next) => {
  res.locals.state = res.locals.state || {}
  res.locals.state.problem = res.locals.state.problem || {}

  if (!req.isAuthenticated()) {
    res.locals.state.problem.results = []
    next()
  }

  const problemId = req.params.id
  const userId = req.user.id
  const combinedId = problemId + ':' + userId

  TestResult
    .query('problemid_userid')
    .eq(combinedId)
    .exec()
    .then((results) => {
      res.locals.state.problem.results = results
      next()
    })
    .catch(next)
}
