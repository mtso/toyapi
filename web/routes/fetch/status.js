export const fetchTestRunningState = (req, res, next) => {
  const isTestRunning = req.user && req.user.requests && req.user.requests.length > 0 || false

  res.locals.state = res.locals.state || {}
  res.locals.state.status = {
    isTestRunning,
  }
  next()
}
