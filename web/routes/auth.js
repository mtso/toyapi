import passport from 'passport'
import { Router } from 'express'
import { savePathContinuation, redirectPathContinuation } from '../utils/pathContinuation'

const auth = Router()

auth.get('/logout',
  (req, res, next) => {
    req.logout()
    res.json({
      error: null,
    })
  },
)

auth.get(
  '/github',
  savePathContinuation,
  passport.authenticate('github'),
)

auth.get('/github/callback',
  passport.authenticate('github', {failureFlash: true}),
  redirectPathContinuation,
)

export default auth
