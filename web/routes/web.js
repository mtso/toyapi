import App from '../../app'
import { renderToString } from 'react-dom/server'
import { StaticRouter } from 'react-router-dom'
import React from 'react'
import { Router } from 'express'
import { Provider } from 'react-redux'
import rootReducer from '../../app/reducers'
import { createStore } from 'redux'
import {
  fetchSessionState,
  fetchProblemState,
  fetchProblemListState,
  fetchTestRunningState,
  fetchProblemResultsState,
} from './fetch'

const web = Router()

web.get(
  '/problem/:id',
  fetchSessionState,
  fetchProblemState,
  fetchTestRunningState,
  renderStore,
  renderApp,
)

web.get(
  '/problem/:id/:tab',
  fetchSessionState,
  fetchProblemState,
  fetchTestRunningState,
  (req, res, next) => {
    if (req.params.tab === 'results') {
      fetchProblemResultsState(req, res, next)
    } else {
      next()
    }
  },
  renderStore,
  renderApp,
)

web.get(
  '/*',
  fetchSessionState,
  fetchProblemListState,
  fetchTestRunningState,
  renderStore,
  renderApp,
)

function renderApp(req, res) {
  const context = {}

  const markup = renderToString(
    <StaticRouter
      context={context}
      location={req.url}
    >
      <Provider
        store={res.locals.store}
      >
        <App />
      </Provider>
    </StaticRouter>,
  )

  if (context.url) {
    res.writeHead(302, {
      Location: context.url,
    })
    res.end()
    return
  }

  res.render(
    'app',
    {
      markup,
      state: JSON.stringify(res.locals.state).replace(/</g, '\\u003c'),
    },
  )
}

function renderStore(req, res, next) {
  const initialState = res.locals.state

  const store = createStore(
    rootReducer,
    initialState,
  )

  res.locals.store = store
  next()
}

export default web
