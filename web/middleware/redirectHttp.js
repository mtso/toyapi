import url from 'url'

export default (req, res, next) => {
  if (req.method !== 'GET' && req.method !== 'HEAD') {
    next()
    return
  }

  const proto = req.get('X-Forwarded-Proto') || ''
  if (proto.split(',')[0] === 'http') {
    const forwardUrl = url.parse(req.originalUrl)
    forwardUrl.protocol = 'https'
    forwardUrl.hostname = process.env.DOMAIN || 'toyapi.com'

    res.redirect(url.format(forwardUrl))
  } else {
    next()
  }
}
