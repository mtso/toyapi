import express from 'express'
import logger from './logger'
import path from 'path'

import middleware from './middleware'
import web from './routes/web'
import auth from './routes/auth'
import apiV1 from './api/v1'

const app = express()

app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, 'views'))

app.use(middleware)
app.use('/auth', auth)
app.use('/api/v1', apiV1)
app.use(web)

const port = process.env.PORT || 3750

app.listen(
  port,
  (err) => err ? logger.error(err) : logger.info('Listening on ' + port),
)
