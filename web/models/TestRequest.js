import orm from 'dynamoose'
import uuid from 'uuid/v4'

// REF: https://stackoverflow.com/a/3809435/2684355
const URL_PATTERN = /^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)$/

const TestRequestSchema = new orm.Schema({
  id: {
    type: String,
    hashKey: true,
    default: () => uuid().replace(/-/g, ''),
  },

  // is_finished: {
  //   type: Boolean,
  //   default: false,
  // },

  'user_id': {
    type: String,
    index: {
      global: true,
      rangeKey: 'id',
      name: 'UserIndex',
      project: true,
      throughput: 1,
    },
  },

  'problem_id': {
    type: String,
    index: {
      global: true,
      rangeKey: 'id',
      name: 'ProblemIndex',
      project: true,
      throughput: 1,
    },
  },

  endpoint: {
    type: String,
    validate: (value) => URL_PATTERN.test(value),
  },

  timestamp: {
    type: Date,
    default: () => new Date(),
  },

  'problemid_userid': {
    type: String,
    index: {
      global: true,
      rangeKey: false,
      name: 'ProblemUser-Index',
      project: true,
      throughput: 1,
    },
  },

})

export default orm.model('ToyApi-TestRequest', TestRequestSchema)
