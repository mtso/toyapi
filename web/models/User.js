import orm from 'dynamoose'
import uuidv4 from 'uuid/v4'

const UserSchema = new orm.Schema({
  id: {
    type: String,
    hashKey: true,
    default: () => uuidv4().replace(/-/g, ''),
  },
  'github_id': {
    type: Number,
    index: {
      global: true,
      rangeKey: 'id',
      name: 'GithubIdIndex',
      project: true,
      throughput: 1,
    },
  },
  username: {
    type: String,
  },
  scopes: {
    type: [String],
  },

  requests: {
    type: [String],
  },
})

const User = orm.model('ToyApi-User', UserSchema, {update: true})

export const createUser = (githubId, username) => {
  const newUser = new User({
    id: uuidv4().replace(/-/g, ''),
    'github_id': githubId,
    username,
    scopes: ['user'],
  })

  return newUser.save()
    .then(() => newUser)
}

export default User
