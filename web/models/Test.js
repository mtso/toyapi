import orm from 'dynamoose'

const TestSchema = new orm.Schema({
  id: {
    type: String,
    hashKey: true,
  },
  name: {
    type: String,
  },
  description: {

  },

  data: {
    type: [Object],
  },
})

export default orm.model('ToyApi-Test', TestSchema)
