import orm from 'dynamoose'
import getSpecMarkup from '../utils/getSpecMarkup'

export const ProblemSchema = new orm.Schema({
  id: {
    type: String,
    hashKey: true,
  },

  order: {
    type: Number,
    index: {
      global: true,
      rangeKey: null,
      name: 'OrderIndex',
      project: true,
      throughput: 1,
    },
  },
  'is_published': {
    type: Boolean,
  },

  'created_at': {
    type: Date,
    index: {
      global: true,
      rangeKey: null,
      name: 'CreatedAtIndex',
      project: true,
      throughput: 1,
    },
  },

  title: {
    type: String,
  },

  description: {
    type: String,
  },

  filename: {
    type: String,
  },

})

const Problem = orm.model('ToyApi-Problem', ProblemSchema)

export default Problem
