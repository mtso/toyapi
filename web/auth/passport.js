import passport from 'passport'
import githubStrategy from './github'
import User from '../models/User'

passport.serializeUser((user, done) => {
  done(null, user.id)
})

passport.deserializeUser((id, done) => {
  User
    .get(id)
    .then((user) => {
      done(null, user)
    })
    .catch(done)
})

passport.use(githubStrategy)

export default passport
