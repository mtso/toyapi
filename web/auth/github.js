import { Strategy as GithubStrategy } from 'passport-github'
import User, { createUser } from '../models/User'

const githubStrategy = new GithubStrategy({
  clientID: process.env.GITHUB_CLIENTID,
  clientSecret: process.env.GITHUB_CLIENTSECRET,
  callbackURL: process.env.HOSTNAME + '/auth/github/callback',
}, (accessToken, refreshToken, profile, done) => {
  const {id, username} = profile

  User
    .query('github_id')
    .eq(id)
    .exec()
    .then((found) => {
      if (found.length < 1) {
        return createUser(id, username)
      }
      return found[0]
    })
    .then((user) => {
      done(null, user)
    })
    .catch(done)
})

export default githubStrategy
