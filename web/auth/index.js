export {default as default} from './passport'

export const isAuthenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
    next()
  } else {
    res.status(400)
      .json({
        error: 'Unauthenticated request',
      })
  }
}

export const isAuthorized = (scopes) => (req, res, next) => {
  if (req.isAuthenticated() && scopes.some((s) => req.user.scopes.includes(s))) {
    next()
  } else {
    res.json({
      error: 'Unauthorized',
    })
  }
}
