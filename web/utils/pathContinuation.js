const cache = {}

export const savePathContinuation = (req, res, next) => {
  const { path } = req.query, { id } = req.session
  cache[id] = path || '/'
  next()
}

export const redirectPathContinuation = (req, res, next) => {
  const { id } = req.session
  const path = cache[id]
  res.redirect(path)
}
