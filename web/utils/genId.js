import uuid from 'uuid/v4'

const genId = () => uuid().replace(/-/g, '')

export default genId
