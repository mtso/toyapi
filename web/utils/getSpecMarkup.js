import fs from 'fs'
import path from 'path'
import marked from 'marked'

// table to cache markup in memory
// [problemFilename : String]
const cache = {}

function getSpecMarkup(filename) {
  if (!cache[filename]) {
    const spec = fs.readFileSync(
      path.resolve(__dirname, `../../data/spec/${filename}.md`),
    ).toString()
    cache[filename] = marked(spec)
  }

  return cache[filename]
}

export default getSpecMarkup
