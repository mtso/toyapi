const uuidv4 = require('uuid/v4')
const dynamoose = require('dynamoose')

function genId() {
  return uuidv4().replace(/-/g, '')
}

const User = dynamoose.model('ToyApi-User', {
  id: {
    type: Number,
    hashKey: true,
  },
  name: {
    type: String,
  },
})

const TestRequest = dynamoose.model('ToyApi-TestRequest', {
  id: {
    type: String,
    hashKey: true,
  },
  // test_id: String,
  user_id: {
    index: {
      global: true,
      rangeKey: 'id',
      name: 'UserIndex',
      project: true,
      throughput: 1,
    },
    type: Number,
  },
})

const TestResult = dynamoose.model('ToyApi-TestResult', {
  id: {
    type: String,
    hashKey: true,
  },
  request_id: {
    type: String,
    index: {
      global: true,
      rangeKey: 'id',
      name: 'TestRequestIndex',
      project: true,
      throughput: 1,
    },
  },
  user_id: {
    index: {
      global: true,
      rangeKey: 'id',
      name: 'UserIndex',
      project: true,
      throughput: 1,
    },
    type: Number,
  },
})

const results = []
const requests = []
Promise.all([
  TestRequest
    .query('user_id')
    .eq(10)
    .exec()
    .then((res) => res.forEach((e) => results.push(e))),
  TestResult
    .query('user_id')
    .eq(10)
    .exec()
    .then((res) => res.forEach((e) => requests.push(e))),
]).then((res) => {
  console.log('results', results)
  console.log('requests', requests)
}).catch(console.error)
return

const result = new TestResult({
  id: genId(),
  request_id: 'a4aa1cc41ca94ec8a3ad57cb6250bf8c',
  user_id: 10,
})

result.save()
  .then(console.log)
  .catch(console.error)
return

// TestRequest
//   .query('user_id')
//   .eq(10)
//   .exec()
//   .then(console.log)
//   .catch(console.error)

// return
return TestRequest
      .query('user_id')
      .eq(10)
      .exec() 
      .then(console.log)
      .catch(console.error)

const requestId = genId()
const request = new TestRequest({
  id: requestId,
  user_id: 10,
})
// const result = new TestResult({
//   id: genId(),
//   request_id: requestId,
// })

const user = new User({
  id: 10,
  name: 'testuser',
})

Promise.all([
  user.save(),
  request.save(),
  // result.save(),
]).then((res) => {
    return TestRequest
      .query('user_id')
      .eq(10)
      .exec()    
  })
  .then((res) => {
    console.log(res)
  })

  .catch(console.error)
