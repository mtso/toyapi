const dynamoose = require('dynamoose')

const Test = dynamoose.model('ToyApi-Test', {
  id: {
    type: String,
    hashKey: true,
  },
  name: {
    type: String,
  },
  order: {
    type: Number,
    index: {
      global: true,
      throughput: 1,
      name: 'order-index',
      project: true,
    },
  },
  tests: {
    type: String,
  },
})

// const TestResult = dynamoose.model('ToyApi-TestResult', {
//   id: {
//     type: String,
//     hashKey: true,
//   },
//   request_id: {
//     type: String,
//     index: {
//       global: true,
//       rangeKey: 'id',
//       name: 'TestRequestIndex',
//       project: true,
//       throughput: 1,
//     },
//   },
//   user_id: {
//     index: {
//       global: true,
//       rangeKey: 'id',
//       name: 'UserIndex',
//       project: true,
//       throughput: 1,
//     },
//     type: Number,
//   },
// })

Test
  .query('order')
  .eq(1)
  // .get('06abcc68806f4a77908585ff678c5db3')
  // .query('id')
  // .eq('06abcc68806f4a77908585ff678c5db3')
  // .query('id')
  // .eq('06abcc68806f4a77908585ff678c5db3')
  // .where('order')
  // .between()
  // .in([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
  // .between(0, 10)
  .exec()
  .then((res) => {
    console.log(res)
  })
  .catch(console.error)

// Promise.all([
//   TestRequest
//     .query('user_id')
//     .eq(10)
//     .exec()
//     .then((res) => res.forEach((e) => results.push(e))),
//   TestResult
//     .query('user_id')
//     .eq(10)
//     .exec()
//     .then((res) => res.forEach((e) => requests.push(e))),
// ]).then((res) => {
//   console.log('results', results)
//   console.log('requests', requests)
// }).catch(console.error)
// return

// const result = new TestResult({
//   id: genId(),
//   request_id: 'a4aa1cc41ca94ec8a3ad57cb6250bf8c',
//   user_id: 10,
// })
