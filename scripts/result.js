'use strict';

const AWS = require('aws-sdk')
const request = require('superagent')
const orm = require('dynamoose')
const TestResult = require('../runner/TestResult')

const testResult = new TestResult({
  request_id: 'test',
  user_id: 'test',
  problem_id: 'test',
  problemid_userid: 'test:test',
  endpoint: 'test',
  results: [
    {
      pass: true,
      description: 'test',
      error: null,
    },
    {
      pass: false,
      description: 'test',
      error: 'test',
    }
  ],
})

testResult
  .save()
  .then(() => {
    console.log('done')
  })
  .catch(console.error)


