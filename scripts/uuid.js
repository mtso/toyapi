const uuidv4 = require('uuid/v4')

function genId() {
  return uuidv4().replace(/-/g, '')
}

console.log(genId())
