// Use babel-node

import dynamoose from 'dynamoose'
import ProblemSchema from '../web/models/Problem'
import uuid from 'uuid/v4'

const Problem = dynamoose.model('ToyApi-Problem', ProblemSchema, {
  update: true,
})

const seedProblems = [
  {
    id: uuid().replace(/-/g, ''),
    created_at: new Date(),
    title: 'Test API',
    description: 'Dummy Test data',
    filename: 'test-api',
    order: 1,
    is_published: true,
  },
  {
    id: uuid().replace(/-/g, ''),
    created_at: new Date(),
    title: 'Echo',
    description: 'Build a server that responds with details about the request.',
    filename: 'echo-api',
    order: 2,
    is_published: false,
  },
  {
    id: uuid().replace(/-/g, ''),
    created_at: new Date(),
    title: 'Sum API',
    description: 'Returns the sum of the Number parts of the URL path.',
    filename: 'sum-api',
    order: 3,
    is_published: false,
  },
]

Problem.batchPut(seedProblems, (err, problems) => {
  console.log(err, problems)
})
