# toyapi

## File Structure
- `app/` common React components and frontend data stores.
- `client/` frontend webpack bundle entry point.
- `web/` Node.js web server that serves the frontend and API.
- `runner/` AWS Lambda test runner.
- `data/` toy API markdown specs and json test cases
- `scripts/` data-seeding scripts, integration sketches
