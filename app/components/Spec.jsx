import React, { Component } from 'react'
import { connect } from 'react-redux'

const mapStateToProps = ({ problem }) => ({
  problem,
})

class Spec extends Component {
  render() {
    const {title, description, specMarkup} = this.props.problem

    const createMarkup = () => ({
      __html: specMarkup,
    })

    return (
      <div className='tab-pane show active'>
        <div className='container tab-container'>
          <span dangerouslySetInnerHTML={createMarkup()}></span>
        </div>
      </div>
    )
  }
}

export default connect(
  mapStateToProps,
)(Spec)
