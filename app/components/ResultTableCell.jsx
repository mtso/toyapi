import React from 'react'

const stringifyPasses = (results) => {
  if (results) {
    const total = results.length
    const passed = results.filter(({pass}) => pass).length
    const passesString = `${ passed } / ${ total }`
    return passesString
  } else {
    return null
  }
}

const ResultTableCell = ({ endpoint, timestamp, results }) => {
  return (
    <tr>
      <td>{ new Date(timestamp).toLocaleString() }</td>
      <td>{ endpoint }</td>
      <td>{ stringifyPasses(results) || 'n/a' }</td>
    </tr>
  )
}

export default ResultTableCell
