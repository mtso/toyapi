import React, { Component } from 'react'
import ProblemListCell from './ProblemListCell'
import { connect } from 'react-redux'
import { loadProblemListAsync } from '../actions'
import { dispatch } from 'redux'

const mapStateToProps = ({ problemList }) => ({
  problemList,
})

const mapDispatchToProps = (dispatch) => ({
  loadProblemList: () => dispatch(loadProblemListAsync())
})

class ProblemList extends Component {
  componentDidMount() {
    const {
      problemList,
      loadProblemList,
    } = this.props

    if (problemList.length < 1) {
      loadProblemList()
    }
  }

  render() {
    const { problemList } = this.props

    const makeCell = (props) => (
      <ProblemListCell
        key={props.id}
        {...props}
      />
    )

    return (
      <div className="container bg-light">
        <div className="toy-list">
          <ul className="list-group">
            { problemList.map(makeCell) }
          </ul>
        </div>
      </div>
    )
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProblemList)
