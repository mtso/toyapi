import React from 'react'
import { Link } from 'react-router-dom'
import { loadDetailAsync } from '../actions'
import { connect } from 'react-redux'
import AsyncLink from './AsyncLink'

const mapDispatchToProps = (dispatch) => ({
  loadDetail: (id) => () => dispatch(loadDetailAsync(id)),
})

const ProblemListCell = ({ id, title, loadDetail }) => (
  <AsyncLink
    to={'/problem/' + id}
    onClick={loadDetail(id)}>
    <li className="list-group-item">{ title }</li>
  </AsyncLink>
)

export default connect(
  null,
  mapDispatchToProps,
)(ProblemListCell)
