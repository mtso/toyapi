import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Link, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

import { onLogoutAsync, startPollingStatus } from '../actions'

const mapStateToProps = ({ session, status }) => ({
  username: session.username,
  isTestRunning: status.isTestRunning,
})

const mapDispatchToProps = (dispatch) => ({
  onLogout: (e) => {
    e.preventDefault()

    return dispatch(onLogoutAsync())
      .catch(console.warn)
  },
  pollStatus: (e) => {
    e.preventDefault()
    startPollingStatus(dispatch)
  },
})

@withRouter
class Nav extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isMenuCollapsed: true,
    }
  }

  toggleMenu = (e) => {
    this.setState({isMenuCollapsed: !this.state.isMenuCollapsed})
  }

  login = (e) => {
    e.preventDefault()
    const loginUrl = '/auth/github?path=' + location.pathname
    location.href = loginUrl
  }

  render() {
    const {username} = this.props
    const {onLogout} = this.props
    const {location} = this.props
    const {isTestRunning} = this.props
    const {pollStatus} = this.props

    const navItemClass = (tab) => {
      let className = 'nav-item'

      if (Array.isArray(tab) && tab.some((t) => location.pathname === t)) {
        className += ' active'
      } else if (location.pathname.includes(tab)) {
        className += ' active'
      }
      return className
    }

    return (
      <nav className="navbar navbar-expand-md navbar-light bg-white">
        <Link to='/' className="navbar-brand" href="#">toyapi</Link>
        <button
          className="navbar-toggler"
          type="button"
          onClick={this.toggleMenu}
        >
          <span className="navbar-toggler-icon"></span>
        </button>

        <div
          className={"collapse navbar-collapse" + (this.state.isMenuCollapsed ? '' : ' show')}
          id="navbarSupportedContent"
        >
          <ul className="navbar-nav mr-auto">

            <li className={'nav-item'}>
              <Link className="nav-link" to="/problems">Problems <span className="sr-only">(current)</span></Link>
            </li>
            <li className={'nav-item'}>
              <Link className="nav-link" to="/about">About</Link>
            </li>
            
            <li className="nav-item">
            
            { !!username ? (
              <div
                className="nav-link link"
                style={{cursor: 'pointer'}}
                onClick={(e) => {
                  onLogout(e)
                }}
              >Log out {username}</div>
            ) : (
              <Link
                className="nav-link"
                to='/auth/github'
                onClick={this.login}
              >Log In with GitHub</Link>
            ) }
            </li>

            { false && (
              <form className="form-inline">
                <button
                  className="btn btn-outline-success my-2 my-sm-0"
                  type="submit"
                  onClick={pollStatus}
                >Check Status</button>
              </form>
            ) }

            { isTestRunning && (
              <li className='nav-item'>
                <div className='nav-link disabled'><div className='loader'></div> Test Running</div>
              </li>
            ) }
          </ul>

        </div>
      </nav>
    )
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Nav)
