import React, { Component } from 'react'
import { connect } from 'react-redux'
import ResultTableCell from './ResultTableCell'

const mapStateToProps = ({ problem }) => ({
  problem,
})

class Results extends Component {
  componentDidMount() {

  }

  render() {
    const { results } = this.props.problem

    return (
      <div className='tab-pane show active'>
        <div className='container tab-container'>

          <table className="table">
            <thead className="thead-light">
              <tr>
                <th scope="col">Timestamp</th>
                <th scope="col">Base URL</th>
                <th scope="col">Passes</th>
              </tr>
            </thead>
            <tbody>
              {results
                .sort((a, b) => new Date(b.timestamp) - new Date(a.timestamp))
                .map((props) => (
                <ResultTableCell
                  key={props.id}
                  {...props}
                />
              ))}
            </tbody>
          </table>

        </div>
      </div>
    )
  }
}

export default connect(
  mapStateToProps,
)(Results)
