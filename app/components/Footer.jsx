import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

const mapStateToProps = ({ }) => ({
})

const mapDispatchToProps = (dispatch) => ({
  
})

class Footer extends Component {
  render() {
    return (
      <footer className='footer'>
        <div className='container'>
          <ul className='list-inline text-muted'>
            <li className='list-inline-item'>ToyAPI</li>
            <li className='list-inline-item'>•</li>
            <li className='list-inline-item'>Made by <a href='https://github.com/mtso'>mtso</a>.</li>
            <li className='list-inline-item'>•</li>
            <li className='list-inline-item'><Link to='/tos'>Terms</Link></li>
            <li className='list-inline-item'>•</li>
            <li className='list-inline-item'><Link to='/faq'>FAQ</Link></li>
          </ul>
        </div>
      </footer>
    )
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Footer)
