import React, { Component } from 'react'
import { connect } from 'react-redux'
import { submitTestRun } from '../actions'

const mapStateToProps = ({ problem, status }) => ({
  problem,
  isTestRunning: status.isTestRunning,
})

const mapDispatchToProps = (dispatch) => ({
  onSubmit: (id) => (endpoint) => dispatch(submitTestRun(id, endpoint)),
})

class Submit extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isInvalidUrl: false,
    }
  }

  onSubmit = (e) => {
    const { isTestRunning } = this.props
    if (isTestRunning) {
      return
    }

    const endpoint = e.target.elements['endpoint'].value
    const { onSubmit } = this.props
    const {id} = this.props.problem

    e.preventDefault()

    this.setState(
      {isInvalidUrl: false},
      () => onSubmit(id)(endpoint).catch(this.onInvalidUrl),
    )
  }

  onInvalidUrl = (err) => {
    this.setState({isInvalidUrl: true})
  }

  render() {
    const {id, title, description} = this.props.problem
    const {onSubmit, isTestRunning} = this.props
    const { isInvalidUrl } = this.state

    const inputClasses = [
      'form-control',
    ]

    const submitClasses = [
      'btn',
      'btn-primary',
    ]

    if (isInvalidUrl) {
      inputClasses.push('is-invalid')
    }

    if (isTestRunning) {
      submitClasses.push('disabled')
    }

    return (
      <div className='tab-pane show active'>
        <div className='container tab-container'>
          <form
            className='tab-content'
            onSubmit={this.onSubmit}
          >
            <div className='form-group'>
              <label htmlFor='base-url'>Base URL</label>
              <input
                className={inputClasses.join(' ')}
                id='base-url'
                type='text'
                name='endpoint'
                placeholder='https://example.glitch.me'
              />
              <small id='url-help' className='form-text text-muted'>
                The integration tests will be run against this base endpoint.
              </small>
            </div>
            
            <input
              className={submitClasses.join(' ')}
              type='submit'
              value='Submit'
            />
            { isTestRunning && (
              <div
                className='loader'
                style={{ marginLeft: '0.4em' }}
              />
            ) }
          </form>
        </div>
      </div>
    )
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Submit)
