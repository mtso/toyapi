function pathEnd(path) {
  let i = path.length - 1
  for (; i >= 0; i--) {
    if (path[i] === '/') {
      break
    }
  }
  return path.slice(i + 1, path.length)
}

export default pathEnd
