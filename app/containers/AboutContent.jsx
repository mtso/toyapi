import React, { Component } from 'react'

class AboutContent extends Component {
  render() {
    return (
      <div className='container'>
        <div className="card no-border">
          <div className="card-body">
            toyapi.com is a list of small backend API projects. Each project
            has its own specification.
          </div>
        </div>
      </div>
    )
  }
}

export default AboutContent
