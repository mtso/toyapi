import React, { Component } from 'react'

class NotFoundContent extends Component {
  render() {
    return (
      <div className='container'>
        <div className="card no-border">
          <div className="card-body">
            404 Not Found.
          </div>
        </div>
      </div>
    )
  }
}

export default NotFoundContent
