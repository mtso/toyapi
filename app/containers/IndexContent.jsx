import React, { Component } from 'react'
import { observer } from 'mobx-react'
import Nav from '../components/Nav'
import ProblemList from '../components/ProblemList'

class IndexContent extends Component {
  render() {
    return (
      <ProblemList />
    )
  }
}

export default IndexContent
