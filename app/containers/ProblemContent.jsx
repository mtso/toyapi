import React, { Component } from 'react'
import Spec from '../components/Spec'
import Submit from '../components/Submit'
import Results from '../components/Results'
import { Link, Switch, Route, Redirect, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import pathEnd from '../utils/pathEnd'
import { loadDetailAsync, onUnloadDetail, loadResultsAsync } from '../actions'
import AsyncLink from '../components/AsyncLink'
import { loadProblemListAsync } from '../actions'

const TABS = {
  SPEC: 'spec',
  SUBMIT: 'submit',
  RESULTS: 'results',
}

const matchStateToProps = ({ problem, session }) => ({
  problem,
  session,
})

const mapDispatchToProps = (dispatch, { history, match }) => ({
  loadProblem: (id) => dispatch(loadDetailAsync(id))
    .catch(console.warn),
  unloadProblem: () => dispatch(onUnloadDetail()),
  loadResults: (id) => () => dispatch(loadResultsAsync(id))
    .catch(console.warn),
  loadProblemList: () => dispatch(loadProblemListAsync()),
})

@withRouter
@connect(
  matchStateToProps,
  mapDispatchToProps,
)
class ProblemContent extends Component {
  componentDidMount() {
    const { problem, loadProblem } = this.props
    const { id } = this.props.match.params

    if (!problem.title) {
      loadProblem(id)
    }
  }

  render() {
    const { id, title, description } = this.props.problem || {}
    const { match, location, unloadProblem, loadResults, session, loadProblemList } = this.props

    const tabClass = (tab) => {
      let className = 'nav-link'

      if (!session.id) {
        switch(tab) {
          case TABS.SUBMIT:
          case TABS.RESULTS:
            className = [className, 'disabled'].join(' ')
            break

          default:
            break
        }
      }

      if (tab === pathEnd(location.pathname)) {
        className += ' active'
      }
      return className
    }

    return (
      <div className="bg-light">
        <AsyncLink
          to='/'
          onClick={loadProblemList}
        >Back to List</AsyncLink>

        <h1>{title}</h1>

        <ul className="nav nav-tabs">
          <li className="nav-item">
            <Link
              to={match.url + '/spec'}
              className={tabClass(TABS.SPEC)}
            >
              Spec
            </Link>
          </li>
          <li className="nav-item">
            <Link
              to={match.url + '/submit'}
              onClick={!!session.id ? null : (e) => e.preventDefault()}
              style={!!session.id ? null : {cursor: 'default'}}
              className={tabClass(TABS.SUBMIT)}
            >
              Submit
            </Link>
          </li>
          <li className="nav-item">
            <AsyncLink
              to={match.url + '/results'}
              onClick={!!session.id ? loadResults(id) : (e) => e.preventDefault()}
              style={!!session.id ? null : {cursor: 'default'}}
              className={tabClass(TABS.RESULTS)}
            >
              Results
            </AsyncLink>
          </li>
        </ul>
        <div className="tab-content bg-white">

          <Switch>
            <Route path='/problem/:id/:tab' render={(props) => {
              const { tab } = props.match.params

              switch(tab) {
                case TABS.SUBMIT:
                  return (<Submit {...props} />)
                case TABS.RESULTS:
                  return (<Results {...props} />)

                case TABS.SPEC:
                default:
                  return (<Spec {...props} />)
              }
            }} />
            <Route path='/problem/:id' render={({ location }) => (
              <Redirect to={location.pathname + '/spec'} />
            )} />
          </Switch>
        </div>

      </div>
    )
  }
}

export default ProblemContent
