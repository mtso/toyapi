import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import IndexContent from './containers/IndexContent'
import ProblemContent from './containers/ProblemContent'
import AboutContent from './containers/AboutContent'
import NotFoundContent from './containers/NotFoundContent'

const Content = () => (
  <div className="container">
    <div className="content">
      <Switch>
        <Route path='/problem/:id' component={ProblemContent} />
        <Route path='/problems' component={IndexContent} />
        <Route path='/about' component={AboutContent} />
        <Route exact path='/' component={IndexContent} />
        <Route path='/' component={NotFoundContent} />
      </Switch>
    </div>
  </div>
)

export default Content
