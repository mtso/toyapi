import request from 'superagent'
import validateUrl from '../utils/validateUrl'
import { apiBaseUrl } from '../config'
import { startPollingStatus } from '.'

export const submitTestRun = (id, endpoint) => (dispatch) => {
  const apiUrl = apiBaseUrl + `/test/${id}`

  const validate = new Promise((resolve, reject) => {
    if (!validateUrl(endpoint)) {
      reject(new Error('Invalid URL'))
    } else {
      resolve()
    }
  })

  return validate
    .then(() => request
      .post(apiUrl)
      .send({endpoint})
      .then((response) => startPollingStatus(dispatch)),
    )
}
