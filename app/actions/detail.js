import request from 'superagent'
import { apiBaseUrl } from '../config'

import {
  ONLOAD_DETAIL,
  ONUNLOAD_DETAIL,
  ONLOAD_RESULTS,
} from './types'

export const loadDetailAsync = (id) => (dispatch) => {
  const endpoint = apiBaseUrl + '/problem/' + id

  return request
    .get(endpoint)
    .then((response) => {
      if (response.statusCode !== 200) {
        throw new Error(response.error)
      } else {
        dispatch(loadDetailAction(response.body))
      }
    })
}

export const loadDetailAction = (problem) => ({
  type: ONLOAD_DETAIL,
  problem,
})

export const onUnloadDetail = () => ({
  type: ONUNLOAD_DETAIL,
})

export const loadResultsAsync = (id) => (dispatch) => {
  const endpoint = apiBaseUrl + '/test/' + id

  return request
    .get(endpoint)
    .then(({ statusCode, body, error }) => {
      if (statusCode === 200) {
        dispatch(loadResultsAction(body))
      } else {
        throw new Error(error)
      }
    })
}

export const loadResultsAction = (results) => ({
  type: ONLOAD_RESULTS,
  results,
})
