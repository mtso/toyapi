import { ONLOGOUT } from './types'
import request from 'superagent'

export const onLogoutAsync = () => (dispatch) => request
  .get('/auth/logout')
  .then((resp) => {
    return resp.body
  })
  .then(({error}) => {
    if (error) {
      throw new Error(error)
    } else {
      dispatch(logoutAction())
    }
  })

export const logoutAction = () => ({
  type: ONLOGOUT,
})
