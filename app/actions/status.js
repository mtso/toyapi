import request from 'superagent'
import { apiBaseUrl } from '../config'
import { ONTESTRUN_START, ONTESTRUN_STOP } from './types'

const POLL_INTERVAL = 5 * 100

export const startPollingStatus = (dispatch) => {
  dispatch(startStatusAction())
  pollStatus(dispatch)
}

function pollStatus(dispatch) {
  request
    .get(apiBaseUrl + '/pending')
    .then((resp) => resp.body)
    .then((runningTests) => {
      if (runningTests.length > 0) {
        setTimeout(
          () => pollStatus(dispatch),
          POLL_INTERVAL,
        )
      } else {
        dispatch(stopStatusAction())
      }
    })
}

export const stopStatusAction = () => ({
  type: ONTESTRUN_STOP,
})

export const startStatusAction = () => ({
  type: ONTESTRUN_START,
})
