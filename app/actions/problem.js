import request from 'superagent'
import { apiBaseUrl } from '../config'

import { ONLOAD_PROBLEMLIST } from './types'

export const loadProblemListAsync = () => (dispatch) => {
  const endpoint = apiBaseUrl + '/problem'

  return request
    .get(endpoint)
    .then((response) => {
      if (response.statusCode !== 200) {
        throw new Error(response.error)
      } else {
        dispatch(loadProblemListAction(response.body))
      }
    })
}

export const loadProblemListAction = (problemList) => ({
  type: ONLOAD_PROBLEMLIST,
  problemList,
})
