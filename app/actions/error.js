import { logoutAction } from '.'

export const onUnauthorized = (dispatch) => (err) => {
  if (err.message === 'Unauthorized') {
    dispatch(logoutAction())
  } else {
    throw err
  }
}
