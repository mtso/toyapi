import React, { Component } from 'react'
import Nav from './components/Nav'
import Footer from './components/Footer'
import Content from './Content'
import { withRouter } from 'react-router-dom'

@withRouter
class App extends Component {
  render() {
    return (
      <div
        className='app-container bg-light'
      >
        <Nav />
        <Content />
        <Footer />
      </div>
    )
  }
}

export default App
