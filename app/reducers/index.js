import { combineReducers } from 'redux'

import session from './session'
import problem from './problem'
import problemList from './problemList'
import status from './status'

export default combineReducers({
  session,
  problem,
  problemList,
  status,
})
