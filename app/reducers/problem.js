import {
  ONLOAD_DETAIL,
  ONUNLOAD_DETAIL,
  ONLOAD_RESULTS,
} from '../actions/types'

const defaultState = {
  id: null,
  title: '',
  description: '',
  specMarkup: '',
  isInvalid: false,
  results: [],
}

export default (state = defaultState, { type, problem, results }) => {
  switch(type) {
  case ONLOAD_DETAIL:
    return Object.assign(
      {},
      state,
      problem,
    )

  case ONUNLOAD_DETAIL:
    return Object.assign(
      {},
      defaultState,
    )

  case ONLOAD_RESULTS:
    return Object.assign(
      {},
      state,
      { results },
    )

  default:
    return state
  }
}
