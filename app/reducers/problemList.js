import { ONLOAD_PROBLEMLIST } from '../actions/types'

const defaultState = []

export default (state = defaultState, { type, problemList }) => {
  switch(type) {
  case ONLOAD_PROBLEMLIST:
    return [].concat(problemList)

  default:
    return state
  }
}
