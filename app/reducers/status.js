import { ONTESTRUN_START, ONTESTRUN_STOP } from '../actions/types'

const defaultState = {
  isTestRunning: false,
}

export default (state = defaultState, { type }) => {
  switch(type) {
  case ONTESTRUN_START:
    return Object.assign(
      {},
      state,
      {isTestRunning: true},
    )
  case ONTESTRUN_STOP:
    return Object.assign(
      {},
      state,
      {isTestRunning: false},
    )
  default:
    return state
  }
}
