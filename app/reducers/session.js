import { ONLOGOUT } from '../actions/types'

const defaultState = {
  username: null,
  id: null,
}

export default (state = defaultState, { type }) => {
  switch(type) {
  case ONLOGOUT:
    return Object.assign(
      {},
      state,
      {username: null},
    )

  default:
    return state
  }
}
