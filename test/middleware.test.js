/*global describe b:true*/
/*global test b:true*/
/*global expect b:true*/

import redirectHttp from '../web/middleware/redirectHttp'

describe('middleware', () => {
  describe('redirectHttp', () => {
    test('should change http to https', (done) => {
      const requestUrl = 'http://toyapi.com/api/problem?page=1&page_count=2'
      const expectedUrl = 'https://toyapi.com/api/problem?page=1&page_count=2'
      const req = {
        method: 'GET',
        originalUrl: requestUrl,
        get: () => 'http,http',
      }

      const res = {
        redirect: (toUrl) => {
          expect(toUrl).toBe(expectedUrl)
          done()
        },
      }

      redirectHttp(
        req,
        res,
        () => { throw new Error('next was called') },
      )
    })
  })
})
