/*global describe b:true*/
/*global beforeAll b:true*/
/*global test b:true*/
/*global expect b:true*/

import fs from 'fs'
import path from 'path'

const datadir = path.resolve(__dirname, '../data/tests')

const readJson = (filename) => new Promise((resolve, reject) => {
  fs.readFile(
    path.join(datadir, filename),
    (err, contents) => {
      if (err) {
        reject(err)
      } else {
        resolve(JSON.parse(contents.toString()))
      }
    },
  )
})

describe('test case data', () => {
  let testdata

  beforeAll((done) => {
    Promise
      .all(
        fs.readdirSync(datadir)
          .map(readJson),
      )
      .then((testObjects) => {
        testdata = testObjects
        Object.freeze(testdata)
        done()
      })
      .catch(done)
  })

  test('should have "headers" field', (done) => {
    testdata
      .forEach((suite) => {
        suite.tests.forEach((testCase) => {
          expect(testCase.request.headers).toBeInstanceOf(Object)
        })
      })
    done()
  })

  test('should have "status" field', (done) => {
    testdata
      .forEach((suite) => {
        suite.tests.forEach((testCase) => {
          expect(typeof testCase.response.status).toBe('number')
        })
      })
    done()
  })

  test('should have "description" field', (done) => {
    testdata
      .forEach((suite) => {
        suite.tests.forEach((testCase) => {
          expect(typeof testCase.description).toBe('string')
        })
      })
    done()
  })

  test('should have "isSync" field', (done) => {
    testdata
      .forEach((suite) => {
        suite.tests.forEach((testCase) => {
          expect(testCase.isSync).toBeDefined()
        })
      })
    done()
  })
})
