const orm = require('dynamoose')
const uuid = require('uuid/v4')

const genId = () => uuid().replace(/-/g, '')

const TestResultSchema = new orm.Schema({
  id: {
    type: String,
    hashKey: true,
    default: genId,
  },
  request_id: {
    type: String,
  },
  user_id: {
    type: String,
    index: {
      global: true,
      rangeKey: 'id',
      name: 'UserIndex',
      project: true,
      throughput: 1,
    },
  },
  problem_id: {
    type: String,
    index: {
      global: true,
      rangeKey: 'id',
      name: 'ProblemIndex',
      project: true,
      throughput: 1,
    }
  },

  problemid_userid: {
    type: String,
    index: {
      global: true,
      rangeKey: null,
      name: 'ProblemIdUserId-Index',
      project: true,
      throughput: 1,
    }
  },
  
  timestamp: {
    type: Date,
    default: () => new Date(),
  },

  results: {
    type: [Object],
  },
  endpoint: {
    type: String,
  },
})

const TestResult = orm.model('ToyApi-TestResult', TestResultSchema)

module.exports = TestResult
