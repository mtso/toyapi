'use strict';

const AWS = require('aws-sdk')
const request = require('superagent')
const orm = require('dynamoose')
const fs = require('fs')
const path = require('path')

const TestResult = require('./TestResult')
const Problem = require('./Problem')

const DATA_DIR = '../data/tests'

// console.log('Loading function');

function trimSlash(url) {
  // FIX for node v6.10?
  url = url.toString()

  return url.replace(/\/$/, '')
}

function runTest(test, endpoint) {
  const method = test.request.method.toLowerCase()
  const pathname = trimSlash(endpoint) + test.request.path
  // console.log('25', test, pathname)

  return new Promise((resolve, reject) => {
    const req = request[method](pathname)
      .set(test.request.query)

    if (test.request.body) {
      req.send(test.request.body)
    }

    return req.then((response) => {
        const isPass = Object.keys(test.response)
          .every((prop) => {
            const expected = test.response[prop]
            const actual = response[prop]

            return expected === actual
          })
      // console.log('38', response)
        resolve({
          pass: isPass,
          description: test.description,
          error: null,
        })
      })
      .catch((err) => {
      // console.log('46', err.message)
        resolve({
          pass: false,
          description: test.description,
          error: err.message,
        })
      })
  })
}

function removeTestRequest(user_id, request_id) {
  return new Promise((resolve, reject) => {
    const ddb = new AWS.DynamoDB.DocumentClient()
    const params = {
      TableName: 'ToyApi-User',
      Key: {id: user_id},
      UpdateExpression: 'DELETE requests :req_id',
      ExpressionAttributeValues: {
        ':req_id': ddb.createSet([request_id]),
      },
      ReturnValues: 'ALL_NEW',
    }

    ddb
      .update(params, (err, data) => {
        if (err) {
          reject(err)
        } else {
          resolve(data)
        }
      })
  })
}

function runSuite(info, done) {
  info = info || {}
  // console.log('68', info)

  const user_id = info.user_id
  const request_id = info.request_id
  const endpoint = info.endpoint
  const problem_id = info.problem_id
  const problemid_userid = info.problemid_userid

  Problem
    .get(problem_id)
    .then((problem) => {
      const filename = problem.filename + '.json'
      const filepath = path.resolve(__dirname, DATA_DIR, filename)

      return new Promise((resolve, reject) => {
        fs.readFile(filepath, (err, data) => {
          // console.log('84', err, data)

          if (err) {
            reject(err)
          } else {
            const testSuite = JSON.parse(data.toString('utf8'))
            const testRuns = testSuite.tests.map((test) => runTest(test, endpoint))

            Promise.all(testRuns)
              .then((results) => resolve(results))
              .catch(reject)
          }
        })
      })
    })
    .then((results) => {
      // console.log('114', results)

      const testResult = new TestResult({
        user_id,
        request_id,
        problem_id,
        problemid_userid,
        endpoint,
        results,
      })

      return Promise.all([
        testResult.save(),
        removeTestRequest(user_id, request_id),
      ])
    })
    .then((results) => {
      // console.log('131', results)

      const snippets = [
        'Successfully saved',
        'REQUEST_ID',
        request_id,
        'PROBLEMID_USERID',
        problemid_userid,
      ]
      done(null, snippets.join(''))
    })
    .catch((err) => done(err.message))
}

exports.handler = (event, context, callback) => {
  if (!event.Records) {
    callback('Invalid event: ' + JSON.stringify(event))
    return
  }

  try {
    if (event.Records.length < 1) {
      callback('No records found in event')
      return
    }
  } catch(e) {
    callback(e.message)
    return
  }

  let user_id
  let endpoint
  let request_id
  let problem_id
  let problemid_userid

  try {
    const object = event.Records[0].dynamodb
    user_id = object.NewImage.user_id.S
    endpoint = object.NewImage.endpoint.S
    request_id = object.NewImage.id.S
    problem_id = object.NewImage.problem_id.S
    problemid_userid = object.NewImage.problemid_userid.S
  } catch(e) {
    callback(e.message)
    return
  }

  try {
    runSuite({
      user_id: user_id,
      endpoint: endpoint,
      request_id: request_id,
      problem_id: problem_id,
      problemid_userid: problemid_userid,
    }, callback)
  } catch(e) {
    callback(Object.prototype.toString.call(request_id) + e.message)
  }
}
