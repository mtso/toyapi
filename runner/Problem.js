const orm = require('dynamoose')

const ProblemSchema = new orm.Schema({
  id: {
    type: String,
    hashKey: true,
  },

  order: {
    type: Number,
    index: {
      global: true,
      rangeKey: null,
      name: 'OrderIndex',
      project: true,
      throughput: 1,
    },
  },
  'is_published': {
    type: Boolean,
  },

  'created_at': {
    type: Date,
    index: {
      global: true,
      rangeKey: null,
      name: 'CreatedAtIndex',
      project: true,
      throughput: 1,
    },
  },

  title: {
    type: String,
  },

  description: {
    type: String,
  },

  filename: {
    type: String,
  },

})

module.exports = orm.model('ToyApi-Problem', ProblemSchema)
