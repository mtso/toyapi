
# Frontend App Paths
```
/toys
/toys/spec
/toys/test
/toys/results

```

# API Endpoints
```
GET /test/:testid/results
GET /tests


GET /api/test/:test_id/results
/api/test/aba23f/results
[
  {
    "timestamp": "TZ",
    "success": true/false,
    "pass_count": 4,
    "fail_count": 0,
    "time_elapsed": 2434, (in ms)
  },
  {
    "timestamp": "TZ",
    "success": true/false,
  }
]

POST /api/test/:test_id
Body {
  user_id: Number,
  base_endpoint: String,
}
```
