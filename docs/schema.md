## User

name | datatype | options
---- | -------- | -------
id   | hash     | 

Test

Api

## TestRequest
name      | datatype | options
--------- | -------- | -------
id        | uuid     | unique
timestamp | datetime |

## TestResult
name       | datatype | options
---------- | -------- | -------
id         | uuid     | unique
timestamp  | datetime |
request_id | uuid     |


