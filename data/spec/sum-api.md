Returns the sum of the integer URL parameters.

## API

### Example Request

```
GET https://sum-example.glitch.me/1/2/3
```

### Example Response

```
6
```
