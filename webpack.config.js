const path = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const config = require('./package.json')

const extractSass = new ExtractTextPlugin({
  filename: 'style.css',
  disable: false,
  allChunks: true,
})

module.exports = [
  {
    entry: {
      bundle: path.resolve(__dirname, 'client'),
    },
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: '[name].js',
    },
    resolve: {
      extensions: [ '.js', '.jsx', '.json' ],
    },
    module: {
      rules: [
        {
          test: /\.jsx?$/,
          include: [
            path.resolve(__dirname, 'app'),
            path.resolve(__dirname, 'client'),
          ],
          loader: 'babel-loader',
          query: config.babel && config.babel.query,
        },
        {
          test: /\.s[ac]ss$/,
          include: [
            path.resolve(__dirname, 'app'),
            path.resolve(__dirname, 'client'),
          ],
          loader: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: 'css-loader!sass-loader',
          }),
        },
      ],
    },
    plugins: [
      extractSass,
    ],
  },
  {
    entry: path.resolve(__dirname, 'web'),
    target: 'node',
    output: {
      path: path.resolve(__dirname, 'build'),
      libraryTarget: 'commonjs',
      filename: 'server.js',
    },
    resolve: {
      extensions: [ '.js', '.jsx', '.json' ],
    },
    externals: [
      /^(?!\.|\/).+/i, // Match any non-relative or absolute paths (npm packages)
    ],
    node: {
      __dirname: true,
      __filename: false, // What does this do
    },
    module: {
      loaders: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          loader: 'babel-loader',
          query: config.babel && config.babel.query,
        },
      ],
    },
  },
]
